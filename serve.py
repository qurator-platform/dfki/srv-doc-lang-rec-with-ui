# serve.py
from flask import Flask
from flask import render_template
from flask import request

import DocumentLanguageRecognizer

# creates a Flask application, named app
app = Flask(__name__)
dlr = DocumentLanguageRecognizer.DLR()

# a route where we will display a welcome message via an HTML template
@app.route("/", methods=['POST','GET'])
def hello():
    if request.method == 'POST':  # this block is only entered when the form is submitted
        text_to_identify_language = request.form['text-to-identify-language']

        # add Maria's method here
        result_language = dlr.run(text_to_identify_language)

        data = {
            'result': result_language,
        }
        return render_template('index.html', **data)

    data = {
            'result': "The Service identifies the language of a text. Please type a text snippet (5-10 words) in English, German, Italian, French, or Spanish.",
    }
    return render_template('index.html', **data)

# run the application
if __name__ == "__main__":
    app.run(debug=True)