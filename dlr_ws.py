'''
Created on 15.03.2019

@author: mamo06
'''
import cherrypy
import DocumentLanguageRecognizer

dlr = DocumentLanguageRecognizer.DLR()

class MyWebService(object):
    
    
    def index(self):
        return ""
    
    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def process(self, input=None): # remove 2nd parameter for POST
        if input == None:
            output = "Please specify an input parameter by appending '/?input=ANY_STRING_in_EN_DE_IT_FR_OR_ES' to the URL in the browser."
        else: 
            # data = cherrypy.request.json # for POST
            output = {"result": dlr.run(input)}

        
        return output
        #return output.to_json()
    
    

    
  
    index.exposed = True


if __name__ == '__main__':
    config = {'server.socket_host': '0.0.0.0'}
    cherrypy.config.update(config)
    cherrypy.quickstart(MyWebService())