from python:3

COPY serve.py .
COPY DocumentLanguageRecognizer.py .
COPY LanguageRanking.py .

COPY static static/
COPY data ./data/
COPY templates templates/

RUN pip install flask

EXPOSE 8080
ENTRYPOINT FLASK_APP=serve.py flask run --host=0.0.0.0 --port=8080